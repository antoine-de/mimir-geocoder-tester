import requests
from invoke import task
import os
import json
import glob


def _create_message():
    """
    create the github comment message

    TODO: 
    * add some metrics about the tests
    * add a comparison with master (and mark the job as failed if needed ?)
    """
    report = None
    for f in glob.glob("**/report.json", recursive=True):
        with open(f, "r") as report:
            report = json.load(report)

    if not report:
        raise Exception("impossible to find report")

    job_url = os.environ.get("CI_JOB_URL")
    artifact_links = f"{job_url}/artifacts/download"

    message = f"""[geocoder-tester](https://github.com/QwantResearch/geocoder-tester) has been run on this pullrequest

The results are:

{report['md_report']}

The tests artifacts can be downloaded [here]({artifact_links}).
    """

    return message


@task(default=True)
def publish_results(ctx, pr_number, github_token):
    message = _create_message()

    body = {"body": message}

    print(f"message = {json}")

    repository = "https://api.github.com/repos/CanalTP/mimirsbrunn"

    url = f"{repository}/issues/{pr_number}/comments"

    r = requests.post(
        url,
        headers={
            "Authorization": f"token {github_token}",
            "Content-Type": "application/json",
        },
        data=json.dumps(body),
    )

    r.raise_for_status()
    print(f"res: {r.json()}")
