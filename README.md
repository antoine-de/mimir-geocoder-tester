# mimir-geocoder-tester

Integration tests for [mimirsbrunn](https://github.com/CanalTP/mimirsbrunn) using [geocoder-tester](https://github.com/geocoders/geocoder-tester) and [docker_mimir](https://github.com/QwantResearch/docker_mimir)

The aim is to run [geocoder tester](https://github.com/geocoders/geocoder-tester) on target [mimir](https://github.com/CanalTP/mimirsbrunn)'s pull requests and have a bot post a summary on the quality of the pull request.

The jobs are run manually, you just have to [run a gitlab pipeline](https://gitlab.com/QwantResearch/mimir-geocoder-tester/pipelines/new) specifying the input variable `PR_NUMBER` with the number of the pull request.

The artifacts on the job contains all the geocoder tester results and [mimir-bot](https://github.com/mimir-bot) posts a github comment on the pull request with the summary of the tests.
