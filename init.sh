#! /bin/bash
set -e
set -x

pip3 install docker-compose pipenv
git clone --depth=1 -b report_json https://github.com/QwantResearch/docker_mimir.git
# git clone --depth=1 -b master https://github.com/QwantResearch/docker_mimir.git
mv *.yml docker_mimir/
cd docker_mimir && pipenv install --system --deploy

# chekint out the mimir version to test
cd ..
git clone --depth=1 -b master https://github.com/CanalTP/mimirsbrunn.git
cd mimirsbrunn
MIMIR_BRANCH_NAME=pr_${PR_NUMBER}
git fetch origin pull/${PR_NUMBER}/head:$MIMIR_BRANCH_NAME
git checkout $MIMIR_BRANCH_NAME
ls
echo `pwd`
cd ..